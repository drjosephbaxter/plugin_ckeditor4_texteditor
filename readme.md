Title: plugin_ckeditor4_texteditor
Author: Dr Joseph Baxter <joseph.baxter@nottingham.ac.uk>
Copyright: University of Nottingham 2017 onwards
Description:

plugin_ckeditor4_texteditor allows Rogō to use the ckeditor 4 text editor https://ckeditor.com/ckeditor-4/

Installation:

1. Extract plugin_ckeditor4_texteditor archive into the plugins/texteditor directory inside Rogō.
2. Install via the plugins/index.php admin screen.